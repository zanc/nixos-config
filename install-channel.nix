import ./unpack-channel.nix {
  name = "nixos-24.11.714876.5d7db4668d7a";
  channelName = "nixos";
  src = builtins.storePath (builtins.fetchurl {
    url = "https://releases.nixos.org/nixos/24.11/nixos-24.11.714876.5d7db4668d7a/nixexprs.tar.xz";
    name = "source";
    sha256 = "0a13nnfr8bhgn67apjgms6d180r4yagd82ai1mdqwar9l2dlgm08";
  });
}
