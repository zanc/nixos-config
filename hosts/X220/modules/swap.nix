{ ... }:

{
  swapDevices = [
    { device = "/swapfile";
      priority = 0;
      size = 1024 * 4;
    }
  ];

  boot.resumeDevice = "/dev/sda1";
  # Use `filefrag /swapfile' to get the resume_offset
  boot.kernelParams = [ "resume_offset=36864" ];
}
