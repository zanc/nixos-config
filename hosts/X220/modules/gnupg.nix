{ lib, ... }:

{
  programs.gnupg.agent.enable = true;
  #programs.gnupg.agent.enableSSHSupport = true;
}
//
(if lib.versionAtLeast (lib.versions.majorMinor lib.version) "24.05"
 then {
   programs.gnupg.agent.pinentryPackage = "gnome3";
 } else {
   programs.gnupg.agent.pinentryFlavor = "gnome3";
 })
