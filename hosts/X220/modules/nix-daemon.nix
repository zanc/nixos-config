{ pkgs, lib, ... }:

(if lib.versionAtLeast (lib.versions.majorMinor lib.version) "22.05"
 then {}
 else { nix.package = pkgs.nixUnstable; })
//
(if lib.versionAtLeast (lib.versions.majorMinor lib.version) "22.11"
 then {
   nix.settings.experimental-features = "nix-command flakes";
   nix.settings.keep-outputs = true;
   #nix.settings.flake-registry = "https://github.com/NixOS/flake-registry/raw/master/flake-registry.json";
   nix.settings.cores = 3;
   nix.settings.max-jobs = 1;
 }
 else {
   nix.extraOptions = ''
     experimental-features = nix-command flakes
     keep-outputs = true
     #flake-registry = https://github.com/NixOS/flake-registry/raw/master/flake-registry.json
   '';
   nix.buildCores = 3;
   nix.maxJobs = 1;
 })
