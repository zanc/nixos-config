{ options, ... }:

{
  #i18n.defaultLocale = "C.UTF-8";
  i18n.supportedLocales = options.i18n.supportedLocales.default ++ [
    "ja_JP.UTF-8/UTF-8"
  ];
}
