{ ... }:

{
  #boot.consoleLogLevel = 1; # equals to `boot.kernelParams = [ "loglevel=1" ];`
  boot.kernelParams = [ "thermal.nocrt=1" ];
}
