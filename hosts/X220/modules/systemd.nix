{ ... }:

{
  #services.journald.extraConfig = "SystemMaxUse=1K";

  # Prevent suspending when the lid is closed
  services.logind.lidSwitch = "ignore";

  systemd.services.systemd-tmpfiles-clean.enable = false;
  systemd.timers.systemd-tmpfiles-clean = {
    enable = false;
    timerConfig = {
      OnUnitActiveSec = "2w"; # run systemd-tmpfiles-clean every 2 weeks
    };
  };
}
