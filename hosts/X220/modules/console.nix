{ options, pkgs, ... }:

{
  console.font = "ter-112n";
  #console.keyMap = "us";
  console.packages = options.console.packages.default ++ [ pkgs.terminus_font ];
}
