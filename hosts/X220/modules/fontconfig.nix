{ options, ... }:

{
  fonts.fontconfig.defaultFonts = {
    emoji = [ "JoyPixels" ];

    # monospace = [
    #   # "Amiri"
    #   "Noto Sans Mono CJK JP"
    # ] ++ options.fonts.fontconfig.defaultFonts.monospace.default;

    # sansSerif = [
    #   # "Amiri"
    #   "Noto Sans CJK JP"
    # ] ++ options.fonts.fontconfig.defaultFonts.sansSerif.default;

    serif = [
      # "Amiri"
      "Noto Serif CJK JP"
    ] ++ options.fonts.fontconfig.defaultFonts.serif.default;
  };
}
