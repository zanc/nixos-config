{ lib, ... }:

{
  boot.loader.grub.enable = true;
  # boot.loader.grub.efiSupport = true;
  # boot.loader.grub.efiInstallAsRemovable = true;
  # boot.loader.efi.efiSysMountPoint = "/boot/efi";
  boot.loader.grub.device = "/dev/sda";
  #boot.loader.grub.extraEntries = ''
  #  menuentry "Wangblows 10" {
  #    chainloader (hd0,1)+1
  #  }
  #'';
}
//
(if lib.versionAtLeast (lib.versions.majorMinor lib.version) "23.05"
then {}
else {
  boot.loader.grub.version = 2;
})
