{ options, config, lib, ... }:

{
  nixpkgs.config =
    if options.nixpkgs.pkgs.isDefined then {}
    else {
      allowUnfree = true;
      permittedInsecurePackages =
        (if lib.versionAtLeast (lib.versions.majorMinor lib.version) "24.11"
         then [ "python-2.7.18.8" "olm-3.2.16" ]
         else if lib.versionAtLeast (lib.versions.majorMinor lib.version) "23.11"
         then [ "python-2.7.18.7" ]
         else if lib.versionAtLeast (lib.versions.majorMinor lib.version) "23.05"
         then [ "python-2.7.18.6" ]
         else []);
      allowInsecurePredicate = pkg: builtins.elem (lib.getName pkg) (map lib.getName (config.nixpkgs.config.permittedInsecurePackages or []));
    };
}
