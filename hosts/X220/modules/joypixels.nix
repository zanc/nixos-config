{ options, lib, ... }:

if lib.versionAtLeast (lib.versions.majorMinor lib.version) "21.05"
then {
  nixpkgs.config =
    if options.nixpkgs.pkgs.isDefined then {}
    else {
      joypixels.acceptLicense = true;
    };
}
else {}
