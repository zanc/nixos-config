{ ... }:

{
  services.openvpn.servers = {
    client = {
      autoStart = false;
      config = ''
        # Comment out auth-user-pass, up, down from the OVPN file
        config /root/openvpn/client.ovpn

        auth-user-pass /root/openvpn/auth-user-pass
      '';
      updateResolvConf = true;
    };
  };
}
