{ pkgs, lib, ... }:

let
  fonts = with pkgs; [
    dejavu_fonts
    freefont_ttf
    gyre-fonts # TrueType substitutes for standard PostScript fonts
    liberation_ttf
    unifont
    #noto-fonts-emoji

    terminus_font
    cantarell-fonts
    (if lib.versionAtLeast (lib.versions.majorMinor lib.version) "22.05"
    then mplus-outline-fonts.githubRelease
    else mplus-outline-fonts)
    powerline-fonts
    joypixels
    siji
    fira-code
    open-sans
    terminus_font_ttf
    inconsolata
    scheherazade
    amiri
    #noto-fonts
    #noto-fonts-extra
    paratype-pt-sans
    noto-fonts-cjk-sans
    noto-fonts-cjk-serif
  ];
in
{
  fonts.enableDefaultFonts = false;
} //
(if lib.versionAtLeast (lib.versions.majorMinor lib.version) "23.11"
then { fonts.packages = fonts; }
else { fonts.fonts = fonts; })
