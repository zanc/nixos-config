{ lib, ... }:

{
  services.dnscrypt-proxy2 = {
    enable = true;
    #configFile = "/etc/dnscrypt-proxy/dnscrypt-proxy.toml";
    settings = {
      #listen_addresses = [];
      #server_names = [ "yandex" ];
    
      #sources.public-resolvers = {
      #  urls = [
      #    "https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/public-resolvers.md"
      #    "https://download.dnscrypt.info/resolvers-list/v3/public-resolvers.md"
      #    "https://ipv6.download.dnscrypt.info/resolvers-list/v3/public-resolvers.md"
      #    "https://download.dnscrypt.net/resolvers-list/v3/public-resolvers.md"
      #  ];
      #  cache_file = "/var/lib/" + config.systemd.services.dnscrypt-proxy2.serviceConfig.StateDirectory + "/public-resolvers.md";
      #  minisign_key = "RWQf6LRCGA9i53mlYecO4IzT51TGPpvWucNSCh1CBM0QTaLn73Y7GFO3";
      #  refresh_delay = 72;
      #  prefix = "";
      #};
    
      #sources.relays = {
      #  urls = [
      #    "https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/relays.md"
      #    "https://download.dnscrypt.info/resolvers-list/v3/relays.md"
      #    "https://ipv6.download.dnscrypt.info/resolvers-list/v3/relays.md"
      #    "https://download.dnscrypt.net/resolvers-list/v3/relays.md"
      #  ];
      #  cache_file = "/var/lib/" + config.systemd.services.dnscrypt-proxy2.serviceConfig.StateDirectory + "/relays.md";
      #  minisign_key = "RWQf6LRCGA9i53mlYecO4IzT51TGPpvWucNSCh1CBM0QTaLn73Y7GFO3";
      #  refresh_delay = 72;
      #  prefix = "";
      #};
    };
  };
}

// (if lib.versionAtLeast (lib.versions.majorMinor lib.version) "21.05"
    then {}
    else {
      systemd.services.dnscrypt-proxy2.serviceConfig = {
        StateDirectory = "dnscrypt-proxy";
      };
    })
