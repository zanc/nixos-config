{ lib, ... }:

if lib.versionAtLeast (lib.versions.majorMinor lib.version) "24.11"
then { hardware.alsa.enablePersistence = true; }
else { sound.enable = true; }
