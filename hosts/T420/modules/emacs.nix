{ ... }:

{
  services.emacs.enable = true;
  services.emacs.startWithGraphical = false;
}
