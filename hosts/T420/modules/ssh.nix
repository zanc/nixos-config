{ ... }:

{
  # Disable x11-ssh-askpass.
  # programs.ssh.askPassword = "";

  programs.ssh.startAgent = true;
}
