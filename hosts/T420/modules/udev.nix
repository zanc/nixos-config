{ pkgs, options, ... }:

{
  hardware.firmware = with pkgs; options.hardware.firmware.default ++ [
    b43Firmware_5_1_138
    #b43Firmware_6_30_163_46
    #b43FirmwareCutter
  ];
}
