{ ... }:

{
  # Enables wireless support via wpa_supplicant.
  networking.wireless.enable = true;
}
