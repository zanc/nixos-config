{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    alacritty
    tmux
    j4-dmenu-desktop
    conky-nox
    nix-index
    vim_configurable
    htop
    git
    mpv
    lftp
    links2
    xbindkeys
    ffmpeg-full
    ungoogled-chromium
    sxiv
    qemu_kvm
    qemu-utils
    cdrkit
    dvdplusrwtools
    libarchive
    squashfsTools
    qutebrowser
    scrot
    gimp
    weechat
    mkvtoolnix-cli
    zip unzip
    dnscrypt-proxy2
    aria2
    unrar
    p7zip
    rename
    font-manager
    exiftool
    hsetroot
    firefox
    librsvg
    gcc
    quilt
    bsdiff
    parcellite
    redshift
    dmenu
    lxappearance
    gtk_engines
    w3m-full
    screenkey
    ntfs3g
    cksfv
    xclip
    megacmd
    psmisc
    xdotool
    xautolock
    imagemagick
    ranger
    poppler_utils
    virtualboxWithExtpack
    libsForQt5.qtstyleplugins
    (if lib.versionAtLeast (lib.versions.majorMinor lib.version) "24.11"
     then libsForQt5.qt5ct
     else qt5ct)
    zathura
    ffmpegthumbnailer
    pass
    patchutils
    wdiff
    dosfstools
    gptfdisk
    syslinux
    mtools
    #fantome-gtk-theme
    arc-theme
    arc-icon-theme
    papirus-icon-theme
    pandoc
    httrack
    hfsprogs
    x11vnc
    dnsutils
    whois
    rclone
    subversion
    mediainfo
    nethogs
    jq
    lm_sensors
    zstd
    i3lock-color
    i3lock-fancy
    wget
    pciutils
    file
    (python2.withPackages (ps: with ps; [ setuptools ]))
    (python3.withPackages (ps: with ps; [ setuptools ]))
    python2Packages.pip
    python3Packages.pip
    binutils
    sqlite
    nixpkgs-manual
    x11_ssh_askpass
    xorg.xmodmap
    xorg.xfd
    #xorg.xinit
    man-pages
    man-pages-posix
    dos2unix
    #gnome3.adwaita-icon-theme
    #xorg.xcursorthemes
    #vanilla-dmz
    irssi
    gnumake
    flashrom coreboot-utils
    usbutils
    dmidecode
    nix-prefetch-git
    xorg.xev
    aegisub
    units
    openssl
    bicon
    torsocks
    cpulimit
    guile_3_0
    #mitscheme
    (if lib.versionAtLeast (lib.versions.majorMinor lib.version) "24.11"
     then transmission_3-gtk
     else transmission-gtk)
    (if lib.versionAtLeast (lib.versions.majorMinor lib.version) "23.11"
    then picom
    else compton)
    #xorg.xclock
    #xorg.fontcursormisc
    xorg.libXcursor
    #gnome3.dconf-editor
    ghc
    cabal-install
    cabal2nix
    stack
    mkpasswd
    patchelf
    termite
    haskellPackages.hoogle
    xmobar_0_44_2
    hlint
    trayer
    gaupol
    (if lib.versionAtLeast (lib.versions.majorMinor lib.version) "22.11"
     then termonad
     else termonad-with-packages)
    inetutils
    nodePackages.js-beautify
    openjdk
    (if lib.versionAtLeast (lib.versions.majorMinor lib.version) "24.11"
     then zenity
     else gnome.zenity)
    lambdabot
    megasync
    wimlib
    woeusb
    texlive.combined.scheme-small
    inotify-tools
    ghostscript
    wineWowPackages.full
    netpbm
    nodejs
    libsForQt5.breeze-qt5
    libsForQt5.plasma-integration
    libsForQt5.kio
    discord
    nss.tools
    tdesktop
    (if lib.versionAtLeast (lib.versions.majorMinor lib.version) "21.11"
     then nixos-option
     else {})
    php
    ruby_3_1
    mailhog
    rxvt-unicode
    ec2-api-tools
    ec2-ami-tools
    cage
    xlsfonts
    xfontsel
    alsa-utils
    fastfetch
  ];

  environment.pathsToLink = [
    "/include"
    "/libexec"
    "/share/fonts"
    "/share/consolefonts"
  ];

  environment.extraOutputsToInstall = [ "dev" ];
}
