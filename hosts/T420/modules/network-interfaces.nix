{ ... }:

{
  networking.hostName = "T420";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;

  networking.interfaces.eno0.useDHCP = true;
  #networking.interfaces.wlp1s0.useDHCP = true;
  networking.interfaces.wlan0.useDHCP = true;
}
