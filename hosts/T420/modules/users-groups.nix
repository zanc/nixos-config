{ options, lib, ... }:

{
  users.users.azure = {
    isNormalUser = true;
    extraGroups = [ "wheel" "adbusers" "wireshark" ];
    hashedPassword = "$6$nBXk9I0dU2$P4UwSPNscclyku/iIku/ga./X6cdTFccbkdHM.7Avvylba4vutiJuqWO1iwyTiXcE7IiknPFu0qG0a6MpiW/i0";
  };
}

// (if lib.versionAtLeast (lib.versions.majorMinor lib.version) "22.05"
    then {
      nix.settings.trusted-users = [ "root" "azure" ];
    }
    else {
      nix.trustedUsers = options.nix.trustedUsers.default ++ [ "azure" ];
    })

// (if lib.versionAtLeast (lib.versions.majorMinor lib.version) "21.05"
    then {
      services.getty.autologinUser = "azure";
    }
    else {
      services.mingetty.autologinUser = "azure";
    })
