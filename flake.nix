{
  inputs = {
    nixpkgs = { url = "github:NixOS/nixpkgs/5d7db4668d7a0c6cc5fc8cf6ef33b008b2b1ed8b"; };
    nixexprs = { url = "https://releases.nixos.org/nixos/24.11/nixos-24.11.714876.5d7db4668d7a/nixexprs.tar.xz"; };
    nixpkgs-overlays = { url = "gitlab:zanc/overlays/83a483faba58a1d9299d7a782b8606f75bd2f3a4"; flake = false; };
    nixos-hardware = { url = "github:NixOS/nixos-hardware/c5013aa7ce2c7ec90acee5d965d950c8348db751"; flake = false; };
    nixus = { url = "github:Infinisil/nixus/2127516f68ce6f900b38289985377559e69aab88"; flake = false; };
  };

  outputs = { self
            , nixpkgs
            , nixexprs
            , nixpkgs-overlays
            , nixos-hardware
            , nixus
            }:

  let

    supportedSystems = [ "x86_64-linux" "i686-linux" "aarch64-linux" ];

    forAllSystems = f: lib.genAttrs supportedSystems (system: f system);

    lib = {
      genAttrs = names: f:
        builtins.listToAttrs (map (n: { name = n; value = f n; }) names);

      mkIf = condition: content:
        { _type = "if";
          inherit condition content;
        };
    };

    attrValues = set:
      builtins.map (x: set.${x}) (builtins.attrNames set);

    flattenAttrs = builtins.foldl' (x: y: x // y) {};

    etcFiles = args@{ path, top ? true, exclude ? [] }:
      let contents = builtins.readDir path; in
      flattenAttrs (attrValues (builtins.mapAttrs (name: value:
        if builtins.elem (path + "/${name}") exclude
        then {}
        else
          if value == "directory"
          then etcFiles ({
            path = path + "/${name}";
            top = false;
          } // builtins.removeAttrs args [ "path" "top" ])
          else {
            "nixos${if top then "" else "/" + builtins.baseNameOf path}/${name}" = {
              source = path + "/${name}";
              mode = "0644";
            };
          })
        contents));

    makeSystem =
      { hostname
      , system
      , modules ? [ (./hosts + "/${hostname}/configuration.nix") ]
      , extraModules ? []
      }:

      let allModules = [
            ({ config, lib, options, pkgs, ... }: {
              # Let 'nixos-version --json' know about the Git revision
              # of this flake.
              system.configurationRevision = lib.mkIf (self ? rev) self.rev;

              # Copy hosts/<hostname>/**/*.nix to /etc/nixos.
              environment.etc = lib.mkMerge [
                (etcFiles {
                  path = ./hosts + "/${hostname}";
                  exclude = [ (./hosts + "/${hostname}/configuration.nix") ];
                })
                {
                  "nixos/configuration.nix" = {
                    source = pkgs.runCommand "configuration.nix" {} ''
                      cat ${./hosts + "/${hostname}/configuration.nix"} >$out
                      sed -i -e 's/#\(@[^@]*@\)/\1/g' $out
                      substituteInPlace $out \
                        --subst-var-by nix.nixPath "nix.nixPath = options.nix.nixPath.default ++ [
                          \"nixos-hardware=${nixos-hardware}\"
                          \"nixpkgs-overlays=${nixpkgs-overlays}/overlays.nix\"
                        ];" \
                        --subst-var-by nixos-hardware "(import \"${nixos-hardware}" \
                        --subst-var-by nixpkgs.overlays "nixpkgs.overlays = import \"${nixpkgs-overlays}/overlays.nix\";"
                    '';
                    mode = "0644";
                  };
                }
              ];

              nixpkgs.pkgs = import nixexprs {
                config = (import (nixpkgs + "/nixos/lib/eval-config.nix") {
                  inherit system modules;
                }).config.nixpkgs.config;
                inherit (config.nixpkgs) localSystem crossSystem;
                overlays = import (nixpkgs-overlays + "/overlays.nix");
              };

              # Make pinned flakes available as <path>.
              nix.nixPath = options.nix.nixPath.default ++ [
                "nixos-hardware=${nixos-hardware}"
                "nixpkgs-overlays=${nixpkgs-overlays}/overlays.nix"
              ];
            })
          ] ++ modules ++ extraModules;
      in {
        "${hostname}" = nixpkgs.lib.nixosSystem {
          inherit system;
          modules = allModules;
        } // { modules = allModules; };
      };

  in {

    nixosConfigurations =
      makeSystem {
        hostname = "T420";
        system = "x86_64-linux";
        extraModules = [
          (nixos-hardware + "/lenovo/thinkpad/t420")
        ];
      }
      //
      makeSystem {
        hostname = "X220";
        system = "x86_64-linux";
        extraModules = [
          (nixos-hardware + "/lenovo/thinkpad/x220")
        ];
      };

    defaultPackage = forAllSystems (system:
      import nixus {
        deploySystem = system;
      } ({ config, ... }: {
        defaults = { name, ... }: {
          nixpkgs = nixexprs;
        };

        nodes.T420 = { lib, config, name, ... }: {
          host = "root@localhost";
          configuration = { imports = self.nixosConfigurations.${name}.modules; };
        };

        nodes.X220 = { lib, config, name, ... }: {
          host = "root@192.168.0.109";
          configuration = { imports = self.nixosConfigurations.${name}.modules; };
        };
      })
    );

  };
}
