#!/usr/bin/env bash

# Workaround for Git CVE-2022-24765:
# https://github.blog/2022-04-12-git-security-vulnerability-announced/
# https://github.com/NixOS/nixpkgs/issues/169193
#
# Run this command:
#   $ ./serve.sh
# Then:
#   # nixos-rebuild switch --flake git+http://0.0.0.0:8000/.git

git update-server-info
python3 -m http.server
