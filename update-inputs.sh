#!/usr/bin/env bash

# Go to https://nixos.org/channels/nixos-x.x to obtain url.
url="https://releases.nixos.org/nixos/24.11/nixos-24.11.714876.5d7db4668d7a"
nixexprs_tar_xz=$(curl -sL "$url" | grep -Po "<a.*?</a>" | grep -o "[^']*/nixexprs\.tar\.xz")
release=$(echo "$nixexprs_tar_xz" | sed -e 's,/[^/]*/[^/]*/\([^/]*\)/[^/]*,\1,' -e 's/nixos-\([^.]*\.[^.]*\)\(\.[^.]*\.[^.]*\)/\1/')
suffix=$( echo "$nixexprs_tar_xz" | sed -e 's,/[^/]*/[^/]*/\([^/]*\)/[^/]*,\1,' -e 's/nixos-\([^.]*\.[^.]*\)\(\.[^.]*\.[^.]*\)/\2/')
revision=$(curl -sL "$url/git-revision")

overlays="83a483faba58a1d9299d7a782b8606f75bd2f3a4"

update_nixexprs() {
    for flake; do
        if [[ ! -f "$flake" ]]; then
            echo >&2 "Warning: $flake doesn't exist"
            continue
        fi
        sed -i -e 's,"https://releases.nixos.org/.*/nixexprs.tar.xz","https://releases.nixos.org/nixos/'"$release"'/nixos-'"$release$suffix"'/nixexprs.tar.xz",' "$flake"
    done
}

update_overlays() {
    for flake; do
        if [[ ! -f "$flake" ]]; then
            echo >&2 "Warning: $flake doesn't exist"
            continue
        fi
        sed -i -e 's,"\(gitlab:zanc/overlays\)/[^"]*","\1/'"$overlays"'",' "$flake"
    done
}

sed -i -e 's,"\(github:NixOS/nixpkgs\)/[^"]*","\1/'"$revision"'",' \
    flake.nix

update_nixexprs flake.nix \
                ../overlays/flake.nix \
                ../nixos-iso-image/flake.nix \
                install-channel.nix

sed -i -e 's,^\(  name = "\)[^"]*,\1'"nixos-$release$suffix"',' install-channel.nix

update_overlays flake.nix \
                ../nixos-iso-image/flake.nix
